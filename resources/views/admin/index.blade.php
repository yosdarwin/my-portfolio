@extends('layout.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Home</h1>
            </div><!-- /.col -->
            
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
     <!-- Main content -->
     <section class="content">
        <div class="container-fluid">          
				{{--Row Slider  --}}
			<div class="row">
				<div class="col-12">
					<button class="btn btn-primary">Add new hero slider</button>
					<table class="table table-striped mt-4">
						<thead>
						  <tr>
							<th scope="col">#</th>
							<th scope="col">Subtitle</th>
							<th scope="col">Title</th>
							<th scope="col">Description</th>
							<th scope="col">Cta 1</th>
							<th scope="col">Cta 1 link</th>
							<th scope="col">Cta 2</th>
							<th scope="col">Cta 2 link</th>
							<th scope="col">Image </th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<th scope="row">1</th>
							<td>Mark</td>
							<td>Otto</td>
							<td>@mdo</td>
							<td>Mark</td>
							<td>Otto</td>
							<td>@mdo</td>
							<td>Mark</td>
							<td>Otto</td>
							
						  </tr>
						 
						</tbody>
					  </table>
				</div>
			</div>        
        </div><!-- /.container-fluid -->
      </section>
      
    </div>
@endsection