@extends('layout.app')
@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image:url(images/xbg_4.jpg.pagespeed.ic.Hn_rroj71A.webp)">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i
                                class="fa fa-chevron-right"></i></a></span> <span>Pricing <i
                            class="fa fa-chevron-right"></i></span></p>
                <h1 class="mb-0 bread">Pricing</h1>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <span class="subheading">Pricing</span>
                <h2>Price &amp; Plans</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 ftco-animate">
                <div class="block-7">
                    <div class="img" style="background-image:url(images/ximage_1.jpg.pagespeed.ic.ptWKaqz1BH.webp)">
                    </div>
                    <div class="p-4">
                        <ul class="pricing-text mb-2">
                            <li><span class="fa fa-check-circle mr-2"></span>Planning Solution</li>
                            <li><span class="fa fa-check-circle mr-2"></span>10 Construction Drawings</li>
                            <li><span class="fa fa-check-circle mr-2"></span>Selection Materials</li>
                            <li><span class="fa fa-check-circle mr-2"></span>Unlimited Revision</li>
                        </ul>
                    </div>
                    <div class="d-lg-flex align-items-center w-100 bg-light py-2 px-4">
                        <span class="price"><sup>$</sup> <span class="number">78</span> <sub>/mos</sub></span>
                        <p class="w-100 mb-0">
                            <a href="#" class="btn btn-primary d-block px-2 py-3">Get Started</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="block-7">
                    <div class="img" style="background-image:url(images/ximage_2.jpg.pagespeed.ic.n9YEkjH9P1.webp)">
                    </div>
                    <div class="p-4">
                        <ul class="pricing-text mb-2">
                            <li><span class="fa fa-check-circle mr-2"></span>Planning Solution</li>
                            <li><span class="fa fa-check-circle mr-2"></span>10 Construction Drawings</li>
                            <li><span class="fa fa-check-circle mr-2"></span>Selection Materials</li>
                            <li><span class="fa fa-check-circle mr-2"></span>Unlimited Revision</li>
                        </ul>
                    </div>
                    <div class="d-lg-flex align-items-center w-100 bg-light py-2 px-4">
                        <span class="price"><sup>$</sup> <span class="number">88</span> <sub>/mos</sub></span>
                        <p class="w-100 mb-0">
                            <a href="#" class="btn btn-primary d-block px-2 py-3">Get Started</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="block-7">
                    <div class="img" style="background-image:url(images/ximage_3.jpg.pagespeed.ic.vYSOuxVQIv.webp)">
                    </div>
                    <div class="p-4">
                        <ul class="pricing-text mb-2">
                            <li><span class="fa fa-check-circle mr-2"></span>Planning Solution</li>
                            <li><span class="fa fa-check-circle mr-2"></span>10 Construction Drawings</li>
                            <li><span class="fa fa-check-circle mr-2"></span>Selection Materials</li>
                            <li><span class="fa fa-check-circle mr-2"></span>Unlimited Revision</li>
                        </ul>
                    </div>
                    <div class="d-lg-flex align-items-center w-100 bg-light py-2 px-4">
                        <span class="price"><sup>$</sup> <span class="number">98</span> <sub>/mos</sub></span>
                        <p class="w-100 mb-0">
                            <a href="#" class="btn btn-primary d-block px-2 py-3">Get Started</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section ftco-services">
    <div class="container">
        <div class="row">
            <div class="col-md-6 d-flex align-self-stretch ftco-animate">
                <div class="pb-4 heading-section heading-section-white">
                    <h2 class="mb-3">We Shape The <br> Perfect Solutions</h2>
                    <p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="services">
                    <div class="d-flex justify-content-end">
                        <div class="icon d-flex"><span class="flaticon-bullhorn"></span></div>
                    </div>
                    <div class="media-body">
                        <h3 class="heading mb-3">Digital <br>Products</h3>
                    </div>
                    <a href="#" class="btn-custom d-flex align-items-center justify-content-center"><span
                            class="fa fa-chevron-right"></span></a>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="services">
                    <div class="d-flex justify-content-end">
                        <div class="icon d-flex"><span class="flaticon-stats"></span></div>
                    </div>
                    <div class="media-body">
                        <h3 class="heading mb-3">Online <br>Marketing</h3>
                    </div>
                    <a href="#" class="btn-custom d-flex align-items-center justify-content-center"><span
                            class="fa fa-chevron-right"></span></a>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="services active">
                    <div class="d-flex justify-content-end">
                        <div class="icon d-flex"><span class="flaticon-vector"></span></div>
                    </div>
                    <div class="media-body">
                        <h3 class="heading mb-3">Graphic <br>Design</h3>
                    </div>
                    <a href="#" class="btn-custom d-flex align-items-center justify-content-center"><span
                            class="fa fa-chevron-right"></span></a>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="services">
                    <div class="d-flex justify-content-end">
                        <div class="icon d-flex"><span class="flaticon-branding"></span></div>
                    </div>
                    <div class="media-body">
                        <h3 class="heading mb-3">Application <br>Development</h3>
                    </div>
                    <a href="#" class="btn-custom d-flex align-items-center justify-content-center"><span
                            class="fa fa-chevron-right"></span></a>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="services">
                    <div class="d-flex justify-content-end">
                        <div class="icon d-flex"><span class="flaticon-web-programming"></span></div>
                    </div>
                    <div class="media-body">
                        <h3 class="heading mb-3">Web <br>Development</h3>
                    </div>
                    <a href="#" class="btn-custom d-flex align-items-center justify-content-center"><span
                            class="fa fa-chevron-right"></span></a>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="services">
                    <div class="d-flex justify-content-end">
                        <div class="icon d-flex"><span class="flaticon-ux"></span></div>
                    </div>
                    <div class="media-body">
                        <h3 class="heading mb-3">UX/UI <br>Design</h3>
                    </div>
                    <a href="#" class="btn-custom d-flex align-items-center justify-content-center"><span
                            class="fa fa-chevron-right"></span></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-intro ftco-section ftco-no-pb">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <div class="img" style="background-image:url(images/xbg_4.jpg.pagespeed.ic.Hn_rroj71A.webp)">
                    <div class="overlay"></div>
                    <h2>Join Us Newsletter</h2>
                    <p>Sign Up to our Newsletter and get our latest news update</p>
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <form action="#" class="subscribe-form">
                                <div class="form-group d-flex">
                                    <input type="text" class="form-control" placeholder="Enter email address">
                                    <input type="submit" value="Subscribe" class="submit px-3">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection