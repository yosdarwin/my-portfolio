@extends('layout.app')

@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image:url(images/xbg_4.jpg.pagespeed.ic.Hn_rroj71A.webp)">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i
                                class="fa fa-chevron-right"></i></a></span> <span>Portfolio <i
                            class="fa fa-chevron-right"></i></span></p>
                <h1 class="mb-0 bread">Portfolio</h1>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 ftco-animate">
                <div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-1.jpg.pagespeed.ic.292SFoWUvn.webp)">
                    <div class="text">
                        <span>Web Application</span>
                        <h3><a href="portfolio-single.html">Interior Design</a></h3>
                        <a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span
                                class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-2.jpg.pagespeed.ic.aUKF3ph13c.webp)">
                    <div class="text">
                        <span>Web Application</span>
                        <h3><a href="portfolio-single.html">Interior Design</a></h3>
                        <a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span
                                class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-3.jpg.pagespeed.ic.plxkfu3M9Z.webp)">
                    <div class="text">
                        <span>Web Application</span>
                        <h3><a href="portfolio-single.html">Interior Design</a></h3>
                        <a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span
                                class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-4.jpg.pagespeed.ic.svBM-UX8pr.webp)">
                    <div class="text">
                        <span>Web Application</span>
                        <h3><a href="portfolio-single.html">Interior Design</a></h3>
                        <a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span
                                class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-5.jpg.pagespeed.ic.ZwmoCptLvV.webp)">
                    <div class="text">
                        <span>Web Application</span>
                        <h3><a href="portfolio-single.html">Interior Design</a></h3>
                        <a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span
                                class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-6.jpg.pagespeed.ic.ZbK9o4zC9a.webp)">
                    <div class="text">
                        <span>Web Application</span>
                        <h3><a href="portfolio-single.html">Interior Design</a></h3>
                        <a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span
                                class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-7.jpg.pagespeed.ic.0zYlepfLjF.webp)">
                    <div class="text">
                        <span>Web Application</span>
                        <h3><a href="portfolio-single.html">Interior Design</a></h3>
                        <a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span
                                class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-8.jpg.pagespeed.ic.MxtxWgeA-X.webp)">
                    <div class="text">
                        <span>Web Application</span>
                        <h3><a href="portfolio-single.html">Interior Design</a></h3>
                        <a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span
                                class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-9.jpg.pagespeed.ic.YH7fGWGoWZ.webp)">
                    <div class="text">
                        <span>Web Application</span>
                        <h3><a href="portfolio-single.html">Interior Design</a></h3>
                        <a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span
                                class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col text-center">
                <div class="block-27">
                    <ul>
                        <li><a href="#">&lt;</a></li>
                        <li class="active"><span>1</span></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-intro ftco-section ftco-no-pb">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <div class="img" style="background-image:url(images/xbg_4.jpg.pagespeed.ic.Hn_rroj71A.webp)">
                    <div class="overlay"></div>
                    <h2>Join Us Newsletter</h2>
                    <p>Sign Up to our Newsletter and get our latest news update</p>
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <form action="#" class="subscribe-form">
                                <div class="form-group d-flex">
                                    <input type="text" class="form-control" placeholder="Enter email address">
                                    <input type="submit" value="Subscribe" class="submit px-3">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection