@extends('layout.postLayout');


@section('content-post')
    <div class="container">
        <div class="row">
            @foreach ($post as $item)
                <div class="col-md-6">
                    <div class="card mb-4">
                        <div class="card-header">
                            {{ $item->title }}
                        </div>
                        <div class="card-body">
                            <div>
                                {{ Str::limit($item->body, 100, '...') }}
                            </div>
                            <a href="/post/{{ $item->slug }}">Read more</a>
                        </div>
                        <div class="card-footer px-4">
                            <small>Publish on {{ $item->created_at->diffForHumans() }}</small>
                        </div>
                    </div>
                </div>
            @endforeach

            {{ $post->links() }}
        </div>
    </div>
@endsection
