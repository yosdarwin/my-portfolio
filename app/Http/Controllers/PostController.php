<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
   public function index()
   {
       $post =  Post::paginate(2);
       return view('posts.index', compact('post'));
   }

   public function show(Post $post)
   {
       return view('posts/show', compact('post'));
   }
}
