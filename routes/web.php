<?php

use App\Http\Controllers\{aboutController, adminController, blogController, contactController, homeController, portfolioController, pricingController, PostController};

use Illuminate\Support\Facades\Route;


Route::get('/', [homeController::class, 'index']);
Route::get('/about', [aboutController::class, 'index']);
Route::get('/pricing', pricingController::class);
Route::get('/portfolio', portfolioController::class);
Route::get('/blog', [blogController::class, 'index']);
Route::get('/contact', contactController::class);
Route::get('/post', [PostController::class, 'index']);
Route::get('/post/{post:slug}', [PostController::class, 'show']);

// Admin 
Route::get('/admin', [adminController::class, 'index']);
